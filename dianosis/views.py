from django.shortcuts import render
from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.decorators import api_view
from .pros import *

# Create your views here.

@api_view(['POST'])
def main(request):
    vomiting = request.data['vomiting']
    nau_or_vom = request.data['nausea_or_vomiting']
    anorexia = request.data['anorexia']
    pain_in_RIF = request.data['pain_in_RIF']
    mig_pain_RIF = request.data['migration_of_pain_to_the_RIF']
    rovsing = request.data['rovsing_sign']
    RIF_tenderness = request.data['RIF_tenderness']
    gender = request.data['gender']
    age = request.data['age']
    rebound = request.data['rebound_tenderness_or_muscular_defense']
    body_tempt = request.data['body_temperature']
    WBC = request.data['WBC']
    leukocytosis_shift = request.data['leukocytosis_shift']
    polymorphonuclear_leukocytes = request.data['polymorphonuclear_leukocytes']
    CRP = request.data['CRP']
    # symptoms_lessthan24h_and_CRP = request.data['symptoms_less_than_24h_and_CRP']
    # symptoms_morethan24h_and_CRP = request.data['symptoms_more_than_24h_and_CRP']
    coughing_hopping_percussion_pain = request.data['coughing_hopping_percussion_pain']
    duration_of_symptoms = request.data['duration_of_symptoms']
    negative_urinalysis = request.data['negative_urinalysis']

    score = ['Vomiting',
              'Nausea or vomiting',
              'Anorexia',
              'Pain in RIF',
              'Migration of pain to the RIF',
              'Rovsing’s sign',
              'RIF tenderness',
              'Rebound tenderness or muscular defense/guarding',
              'Body temperature',
              'WBC (white blood cell) count',
              'Leukocytosis shift',
              'Polymorphonuclear leukocytes',
              'CRP (C-reactive protein) concentration',
              'Symptoms <24 h and CRP (C-reactive protein) concentration',
              'Symptoms >24 h and CRP (C-reactive protein) concentration',
              'Coughing/hopping/percussion pain',
              'gender',
              'age',
              'Duration of symptoms',
              'Negative urinalysis'
              ]

    Alvarado_score = dict.fromkeys(score, 0)
    AIR_score = dict.fromkeys(score, 0)
    PAS_score = dict.fromkeys(score, 0)
    RIPASA_score = dict.fromkeys(score, 0)
    AAS_score = dict.fromkeys(score, 0)

    if vomiting:
        AIR_score[score[0]] = 1
        
    if nau_or_vom:
        Alvarado_score[score[1]] = 1
        PAS_score[score[1]] = 1
        RIPASA_score[score[1]] = 1
    
    if anorexia:
        Alvarado_score[score[2]] = 1
        PAS_score[score[2]] = 1
        RIPASA_score[score[2]] = 1
    
    if pain_in_RIF:
        Alvarado_score[score[3]] = 2
        AIR_score[score[3]] = 1
        RIPASA_score[score[3]] = 0.5
        AAS_score[score[3]] = 2
    
    if mig_pain_RIF:
        Alvarado_score[score[4]] = 1
        PAS_score[score[4]] = 1
        RIPASA_score[score[4]] = 0.5
        AAS_score[score[4]] = 2
    
    if rovsing:
        RIPASA_score[score[5]] = 2
    
    if RIF_tenderness:
        PAS_score[score[6]] = 2
        RIPASA_score[score[6]] = 1
        if age>50 or gender=='Male':
            AAS_score[score[6]] = 3
        else:
            AAS_score[score[6]] = 1
    
    if rebound is not None:
        Alvarado_score[score[7]] = 1
        RIPASA_score[score[7]] = 3
        if rebound == 'Light':
            AIR_score[score[7]] = 1
            AAS_score[score[7]] = 2
        elif rebound == 'Medium':
            AIR_score[score[7]] = 2
            AAS_score[score[7]] = 4
        elif rebound == 'Strong':
            AIR_score[score[7]] = 3
            AAS_score[score[7]] = 4
    
    if body_tempt > 37.5:
        Alvarado_score[score[8]] = 1
        PAS_score[score[8]] = 1
    if body_tempt > 38.5:
        AIR_score[score[8]] = 1
    if body_tempt>37 and body_tempt<39:
        RIPASA_score[score[8]] = 1
    
    if WBC > 10e9:
        Alvarado_score[score[9]] = 2
        PAS_score[score[9]] = 1
        RIPASA_score[score[9]] = 1
    if WBC>=10e9 and WBC<=14.9e9:
        AIR_score[score[9]] = 1
    if WBC >= 15e9:
        AIR_score[score[9]] = 2
    if WBC >= 0.2e9 and WBC < 10.9e9:
        AAS_score[score[9]] = 1
    if WBC >= 10.9e9 and WBC < 14e9:
        AAS_score[score[9]] = 2
    if WBC >= 14e9:
        AAS_score[score[9]] = 3
    
    if polymorphonuclear_leukocytes >= 70:
        Alvarado_score[score[10]] = 1
    
    if polymorphonuclear_leukocytes >= 70 and polymorphonuclear_leukocytes <= 84:
        AIR_score[score[11]] = 1
    if polymorphonuclear_leukocytes >= 75:
        PAS_score[score[11]] = 1
    if polymorphonuclear_leukocytes >= 85:
        AIR_score[score[11]] = 2
    if polymorphonuclear_leukocytes >= 62 and polymorphonuclear_leukocytes < 75:
        AAS_score[score[11]] = 2
    if polymorphonuclear_leukocytes >= 75 and polymorphonuclear_leukocytes < 83:
        AAS_score[score[11]] = 3
    if polymorphonuclear_leukocytes >= 83:
        AAS_score[score[11]] = 4
    
    if CRP >=10 and CRP < 50:
        AIR_score[score[12]] = 1
    if CRP >= 50:
        AIR_score[score[12]] = 2

    if duration_of_symptoms <24:
        symptoms_lessthan24h_and_CRP = CRP
        if symptoms_lessthan24h_and_CRP >= 4 and symptoms_lessthan24h_and_CRP < 11:
            AAS_score[score[13]] = 2
        if symptoms_lessthan24h_and_CRP >= 11 and symptoms_lessthan24h_and_CRP < 25:
            AAS_score[score[13]] = 3
        if symptoms_lessthan24h_and_CRP >= 25 and symptoms_lessthan24h_and_CRP < 83:
            AAS_score[score[13]] = 5
        if symptoms_lessthan24h_and_CRP >= 83:
            AAS_score[score[13]] = 1

    if duration_of_symptoms >= 24:
        symptoms_morethan24h_and_CRP =CRP
        if symptoms_morethan24h_and_CRP >= 12 and symptoms_morethan24h_and_CRP < 152:
            AAS_score[score[14]] = 2
        if symptoms_morethan24h_and_CRP >= 152:
            AAS_score[score[14]] = 1

    if coughing_hopping_percussion_pain:
        PAS_score[score[15]] = 2

    if gender == 'Male':
        RIPASA_score[score[16]] = 1
    if gender == 'Female':
        RIPASA_score[score[16]] = 0.5

    if age < 40:
        RIPASA_score[score[17]] = 1
    if age >= 40:
        RIPASA_score[score[17]] = 0.5

    if duration_of_symptoms < 48:
        RIPASA_score[score[18]] = 1
    if duration_of_symptoms >= 48:
        RIPASA_score[score[18]] = 0.5

    if negative_urinalysis:
        RIPASA_score[score[19]] = 1

    t_Alvarado_score = sum(Alvarado_score.values())
    t_AIR_score = sum(AIR_score.values())
    t_PAS_score = sum(PAS_score.values())
    t_RIPASA_score = sum(RIPASA_score.values())
    t_AAS_score = sum(AAS_score.values())

    if t_Alvarado_score < 5:
        Alvarado_diagnosis = 'Not likely appendicitis'
    if t_Alvarado_score >= 5 and t_Alvarado_score < 7:
        Alvarado_diagnosis = 'Equivocal'
    if t_Alvarado_score >=7 and t_Alvarado_score < 9:
        Alvarado_diagnosis = 'Probably appendicitis'
    if t_Alvarado_score >=9:
        Alvarado_diagnosis = 'Highly likely appendicitis'

    if t_AIR_score < 5:
        AIR_diagnosis = 'Low probability'
    if t_AIR_score >= 5 and t_AIR_score < 9:
        AIR_diagnosis = 'Indeterminate group'
    if t_AIR_score >= 9:
        AIR_diagnosis = 'High probability'

    if t_PAS_score >= 6:
        PAS_diagnosis = 'Appendicitis'
    if t_PAS_score < 6:
        PAS_diagnosis = 'Observe'

    if t_AAS_score < 11:
        AAS_diagnosis = 'Low risk'
    if t_AAS_score >= 11 and t_AAS_score < 16:
        AAS_diagnosis = 'Intermediate risk'
    if t_AAS_score >= 16:
        AAS_diagnosis = 'High risk'

    predict = {
        'Alvarado_score': {
            'Score': t_Alvarado_score,
            'Diagnosis': Alvarado_diagnosis
        },
        'AIR_score': {
            'Score': t_AIR_score,
            'Diagnosis': AIR_diagnosis
        },
        'PAS_score': {
            'Score': t_PAS_score,
            'Diagnosis':PAS_diagnosis
        },
        'AAS_score': {
            'Score': t_AAS_score,
            'Diagnosis': AAS_diagnosis
        }
    }


    return JsonResponse(predict)