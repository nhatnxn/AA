from django.apps import AppConfig


class DianosisConfig(AppConfig):
    name = 'dianosis'
